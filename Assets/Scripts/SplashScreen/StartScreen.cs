﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
namespace SplashScreen
{
    public class StartScreen : MonoBehaviour
    {
        //Editor
        [SerializeField] private float animationUITime;
        [SerializeField] private ProgressBar progressBar;
        [SerializeField] private Canvas SplashScreenCanvas;
        [SerializeField] private RectTransform splashScreenUI;

        void Start()
        {
            DontDestroyOnLoad(this.gameObject);
            DontDestroyOnLoad(SplashScreenCanvas.gameObject);
            StartCoroutine(LoadGameScene());
        }

        IEnumerator LoadGameScene()
        {
            var op = SceneManager.LoadSceneAsync(1);
            while (!op.isDone)
            {
                progressBar.UpdateProgress(op.progress * 100f);
                yield return new WaitForSeconds(Time.deltaTime);
            }
            progressBar.UpdateProgress(99f);
            yield return new WaitForFixedUpdate();
            Debug.Log("[SplashScreen.LoadGameScene]: Scene loaded");
            var targetWidth = -splashScreenUI.rect.width;
            var animTime = 0f;
            while (animTime <= animationUITime)
            {
                var rightVector = splashScreenUI.offsetMax;
                var progress = animTime / (animationUITime - Time.deltaTime);
                rightVector.x = Mathf.Lerp(0, targetWidth, progress);
                var leftVector = splashScreenUI.offsetMin;
                leftVector.x = Mathf.Lerp(0, targetWidth, progress);
                splashScreenUI.offsetMax = rightVector;
                splashScreenUI.offsetMin = leftVector;
                animTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            Destroy(SplashScreenCanvas.gameObject);
            Destroy(this.gameObject);
        }
    }
}
