﻿using TMPro;
using UnityEngine;

namespace SplashScreen
{
    public class ProgressBar : MonoBehaviour
    {
        //Editor
        [SerializeField] private RectTransform background;
        [SerializeField] private RectTransform slider;
        [SerializeField] private TMP_Text progressNumberText;

        //Private
        private bool _inited;
        private float _sliderWidth;

        private void Awake()
        {
            Init();
        }

        void Init()
        {
            if (_inited) return;
            _inited = true;
            _sliderWidth = background.rect.width;
            UpdateProgress(0);
        }

        public void UpdateProgress(float value, float maxValue = 100f)
        {
            if (!_inited) Init();
            float clamped_t = Mathf.Clamp01(value / maxValue);
            slider.offsetMax = new Vector2(Mathf.Lerp(-_sliderWidth, 0, clamped_t), slider.offsetMax.y);
            progressNumberText.text = (int)value + "%";
        }
    }
}
