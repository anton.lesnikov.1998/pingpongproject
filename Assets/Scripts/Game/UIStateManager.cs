﻿using Game.Enums;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class UIStateManager : MonoBehaviour
    {
        //Editor
        [SerializeField] private EGameStates InitialState;
        [SerializeField] private List<GameUIElement> UIElements;

        //Public
        public EGameStates CurrentState
        {
            get => _currentState; set
            {
                _currentState = value;
                OnUIStateChanged(value);
            }
        }
        public Action<EGameStates> OnStateChangedEvent;

        //Private
        private EGameStates _currentState;

        //Private Static
        private static UIStateManager _instance;
        

        private void Awake()
        {
            _instance = this;
            CurrentState = InitialState;
        }

        public static UIStateManager Get()
            => _instance;

        void OnUIStateChanged(EGameStates newState)
        {
            foreach (var elem in UIElements)
            {
                var isShow = elem.ShowStates.Contains(newState);
                elem.Show(isShow);
            }
            OnStateChangedEvent?.Invoke(newState);
        }

        private void OnDestroy()
        {
            _instance = null;
        }
    }
}
