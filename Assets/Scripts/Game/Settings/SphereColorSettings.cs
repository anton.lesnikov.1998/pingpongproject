﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.Settings
{
    public class SphereColorSettings : MonoBehaviour
    {
        //Editor
        [SerializeField] private Slider redSlider;
        [SerializeField] private Slider greenSlider;
        [SerializeField] private Slider blueSlider;
        [SerializeField] private Image previewImage;

        //Private
        private float red, green, blue;

        private void Start()
        {
            redSlider.onValueChanged.AddListener(OnRedChanged);
            greenSlider.onValueChanged.AddListener(OnGreenChanged);
            blueSlider.onValueChanged.AddListener(OnBlueChanged);

            if (PlayerPrefs.HasKey(SaveDataKeyWords.SPHERE_COLOR_RED)
                && PlayerPrefs.HasKey(SaveDataKeyWords.SPHERE_COLOR_GREEN)
                && PlayerPrefs.HasKey(SaveDataKeyWords.SPHERE_COLOR_BLUE))
            {
                red = PlayerPrefs.GetFloat(SaveDataKeyWords.SPHERE_COLOR_RED);
                green = PlayerPrefs.GetFloat(SaveDataKeyWords.SPHERE_COLOR_GREEN);
                blue = PlayerPrefs.GetFloat(SaveDataKeyWords.SPHERE_COLOR_BLUE);
            }
            else
            {
                red = 0;
                green = 0;
                blue = 0;
            }
            OnColorChanged();
        }

        void OnRedChanged(float value)
        {
            red = value;
            OnColorChanged();
        }

        void OnGreenChanged(float value)
        {
            green = value;
            OnColorChanged();
        }

        void OnBlueChanged(float value)
        {
            blue = value;
            OnColorChanged();
        }

        void OnColorChanged()
        {
            previewImage.color = new Color(red, green, blue);
            PlayerPrefs.SetFloat(SaveDataKeyWords.SPHERE_COLOR_RED, red);
            PlayerPrefs.SetFloat(SaveDataKeyWords.SPHERE_COLOR_GREEN, green);
            PlayerPrefs.SetFloat(SaveDataKeyWords.SPHERE_COLOR_BLUE, blue);
            PlayerPrefs.Save();
        }
    }
}
