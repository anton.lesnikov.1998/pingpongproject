﻿using Game.Enums;
using UnityEngine;

namespace Game
{
    public class SphereLogic : MonoBehaviour
    {
        //Editor
        [SerializeField] private Vector2 minMaxSpeed;
        [SerializeField] private Vector2 minMaxSize;
        [SerializeField] private float resetTime;
        [SerializeField] private Renderer rend;
        [SerializeField] private Rigidbody rb;

        //Private
        private Vector3 _startPosition;
        private bool _movingDown, _gameState;
        private Collision _LastPlayerCollision;

        private void Start()
        {
            _startPosition = transform.position;
            ResetSphere();
            UIStateManager.Get().OnStateChangedEvent += OnGameStateChanged;
        }

        void OnGameStateChanged(EGameStates newState)
        {
            UpdateSphereColor();
            ResetSphere();
            ResetSize();
            if (newState != EGameStates.STATE_GAME)
            {
                _gameState = false;
                return;
            }
            _gameState = true;
            Invoke(nameof(AddRandomImpulse), resetTime);
        }

        void UpdateSphereColor()
        {
            //If any part of the data is missing, skip
            if (!PlayerPrefs.HasKey(SaveDataKeyWords.SPHERE_COLOR_RED)
                || !PlayerPrefs.HasKey(SaveDataKeyWords.SPHERE_COLOR_GREEN)
                || !PlayerPrefs.HasKey(SaveDataKeyWords.SPHERE_COLOR_BLUE))
                return;
            var red = PlayerPrefs.GetFloat(SaveDataKeyWords.SPHERE_COLOR_RED);
            var green = PlayerPrefs.GetFloat(SaveDataKeyWords.SPHERE_COLOR_GREEN);
            var blue = PlayerPrefs.GetFloat(SaveDataKeyWords.SPHERE_COLOR_BLUE);
            rend.sharedMaterial.color = new Color(red, green, blue);
        }


        float GetRandomSpeed()
            => Random.Range(minMaxSpeed.x, minMaxSpeed.y);

        float GetRandomSide()
            => (Random.Range(0, 2) == 0) ? 1 : -1;

        void AddRandomImpulse()
        {
            if (!_gameState) return;
            rb.isKinematic = false;

            var startSpeed = GetRandomSpeed();
            var startVector = new Vector3(_movingDown ? 1 : -1, 0, GetRandomSide());

            rb.AddForce(startVector * startSpeed);
        }

        void AddLeftRightImpulse()
        {
            var impulseSpeed = GetRandomSpeed();
            rb.AddForce(new Vector3(0, 0, GetRandomSide() * impulseSpeed));
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (_LastPlayerCollision == collision) return;
            if (collision.gameObject.CompareTag("Player"))
            {
                _movingDown = !_movingDown;
                AddLeftRightImpulse();
                AddRandomImpulse();
            }
            if (collision.gameObject.CompareTag("PlayerWall"))
            {
                var scoreManager = ScoreManager.Get();
                if (_movingDown)
                    scoreManager.LosePoints();
                else
                    scoreManager.AddPoint();
                ResetSphere();
                Invoke(nameof(AddRandomImpulse), resetTime);
            }
        }

        void ResetSize()
        {
            var size = Random.Range(minMaxSize.x, minMaxSize.y);
            transform.localScale = new Vector3(size, size, size);
            // var pos = transform.position;
            // pos.y += size / 2f;
            //transform.position = pos;
        }

        void ResetSphere()
        {
            _movingDown = Random.Range(0, 2) == 0;

            _LastPlayerCollision = null;
            rb.velocity = Vector3.zero;
            transform.position = _startPosition;
            rb.isKinematic = true;
        }

        private void FixedUpdate()
        {
            var velocityVector = rb.velocity;
            if (velocityVector.x > -minMaxSpeed.x && velocityVector.x < minMaxSpeed.x)
            {
                velocityVector.y = 0;
                velocityVector.x = _movingDown ? minMaxSpeed.x : -minMaxSpeed.x;
                rb.velocity = velocityVector;
            }
        }

        private void OnDestroy()
        {
            var stateManager = UIStateManager.Get();
            if (stateManager != null)
                stateManager.OnStateChangedEvent -= OnGameStateChanged;
        }
    }
}
