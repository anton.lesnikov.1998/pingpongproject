﻿
namespace Game
{
    public static class SaveDataKeyWords
    {
        public static string SPHERE_COLOR_RED ="SPHERE_RED";
        public static string SPHERE_COLOR_GREEN = "SPHERE_GREEN";
        public static string SPHERE_COLOR_BLUE = "SPHERE_BLUE";
        public static string HIGH_SCORE = "HIGH_SCORE";
    }
}
