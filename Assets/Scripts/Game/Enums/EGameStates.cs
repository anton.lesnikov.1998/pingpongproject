﻿namespace Game.Enums
{
    public enum EGameStates
    {
        STATE_MENU,
        STATE_GAME,
        STATE_SETTINGS
    }
}
