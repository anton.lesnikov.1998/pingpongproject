﻿using Game.Enums;
using UnityEngine;

namespace Game
{
    public class Player : MonoBehaviour
    {
        //Editor
        [SerializeField] private float speed;
        [SerializeField] private float maxMoveDistance = 3.5f;

        //Private 
        private float _middleScreenX;
        private bool _isMoveState, _isMoving;

        private void Start()
        {
            _middleScreenX = Screen.width / 2f;
            UIStateManager.Get().OnStateChangedEvent += OnGameStateChanged;
        }

        void OnGameStateChanged(EGameStates newGameState)
            => _isMoveState = newGameState == EGameStates.STATE_GAME;

        void Move(bool isLeft)
        {
            //Check if the limits have been reached
            var pos = transform.position;
            if (isLeft && pos.z < -maxMoveDistance
                || !isLeft && pos.z > maxMoveDistance) return;

            var moveSpeed = speed * Time.deltaTime;
            if (isLeft)
                moveSpeed *= -1f;
            pos.z += moveSpeed;
            transform.position = pos;
        }

        void PlayerLogic()
        {
            if (Input.mousePosition.x < _middleScreenX)
                Move(true);
            else
                Move(false);
        }

        private void Update()
        {
            //PlayerInput
            if (Input.GetMouseButtonDown(0))
                _isMoving = true;
            else if (Input.GetMouseButtonUp(0))
                _isMoving = false;
        }

        private void FixedUpdate()
        {
            //Valid Game State Checking
            if (!_isMoveState) return;

            if (_isMoving)
                PlayerLogic();
        }

        private void OnDestroy()
        {
            var stateManager = UIStateManager.Get();
            if (stateManager != null)
                stateManager.OnStateChangedEvent -= OnGameStateChanged;
        }
    }
}
