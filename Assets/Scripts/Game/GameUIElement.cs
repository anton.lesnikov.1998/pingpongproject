﻿using Game.Enums;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class GameUIElement : MonoBehaviour
    {
        //Editor
        public List<EGameStates> ShowStates;

        public void Show(bool show)
            => gameObject.SetActive(show);
    }
}
