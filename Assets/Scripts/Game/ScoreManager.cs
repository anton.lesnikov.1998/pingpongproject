﻿using Game.Enums;
using TMPro;
using UnityEngine;

namespace Game
{
    public class ScoreManager : MonoBehaviour
    {
        //Editor
        [SerializeField] private TMP_Text scoreText;
        [SerializeField] private TMP_Text hightscoreText;

        //Private
        private int _score, _highScore;

        //Private Static
        private static ScoreManager _instance;
        private static string SCORE = "Score: ";
        private static string HIGH_SCORE = "HighScore: ";

        private void Awake()
        {
            _instance = this;
        }

        private void Start()
        {
            UIStateManager.Get().OnStateChangedEvent += OnGameStateChanged;
            if (PlayerPrefs.HasKey(SaveDataKeyWords.HIGH_SCORE))
                _highScore = PlayerPrefs.GetInt(SaveDataKeyWords.HIGH_SCORE);
            UpdateText();
        }

        void OnGameStateChanged(EGameStates newState)
        {
            _score = 0;
        }

        public static ScoreManager Get()
            => _instance;

        public void AddPoint()
        {
            _score++;
            if (_score > _highScore)
            {
                _highScore = _score;
                PlayerPrefs.SetInt(SaveDataKeyWords.HIGH_SCORE, _highScore);
            }
            UpdateText();
        }

        public void LosePoints()
        {
            _score = 0;
            UpdateText();
        }

        void UpdateText()
        {
            scoreText.text = SCORE + _score;
            hightscoreText.text = HIGH_SCORE + _highScore;
        }

        private void OnDestroy()
        {
            var stateManager = UIStateManager.Get();
            if (stateManager != null)
                stateManager.OnStateChangedEvent -= OnGameStateChanged;
        }
    }
}
