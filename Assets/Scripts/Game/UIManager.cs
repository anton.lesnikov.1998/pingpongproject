﻿using Game.Enums;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class UIManager : MonoBehaviour
    {
        //Editor
        [SerializeField] private Button playButton;
        [SerializeField] private Button returnButton;
        [SerializeField] private Button settingsButton;

        private void Start()
        {
            playButton.onClick.AddListener(OnPlayButton);
            returnButton.onClick.AddListener(OnReturnButton);
            settingsButton.onClick.AddListener(OnSettingsButton);
        }

        void OnPlayButton()
            => UpdateState(EGameStates.STATE_GAME);

        void OnReturnButton()
            => UpdateState(EGameStates.STATE_MENU);

        void OnSettingsButton()
            => UpdateState(EGameStates.STATE_SETTINGS);

        void UpdateState(EGameStates state)
        {
            var stateManager = UIStateManager.Get();
            if (stateManager != null)
                stateManager.CurrentState = state;
        }
    }
}
